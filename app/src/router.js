import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/faturaForm',
      name: 'faturaFormCadastro',
      component: () => import('./views/faturaForm.vue')
    },
    {
      path: '/faturaForm/:id',
      name: 'faturaFormEdicao',
      component: () => import('./views/faturaForm.vue')
    }
  ]
})
