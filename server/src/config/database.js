const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose
    .connect('mongodb://localhost:27017/avaliacao_tilix', { useNewUrlParser: true })
    .catch(resp => console.log('Erro:', resp));

module.exports = mongoose