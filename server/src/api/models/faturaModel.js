const mongoose = require('../../config/database')

const faturaSchema = new mongoose.Schema({
    idfatura: {
        type: Number, required: true
    },
    idcliente: {
        type: Number, required: true
    },
    nome_empresa: {
        type: String, required: true
    },
    valor: {
        type: Number, required: true
    },
    data_vencimento: {
        type: Date, required: true
    },
    pagou: {
        type: Boolean, required: true, default: false
    }
});

module.exports = mongoose.model('fatura', faturaSchema);