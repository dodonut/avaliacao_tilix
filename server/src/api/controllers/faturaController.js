const Fatura = require('../models/faturaModel');
const express = require('express')
const router = express.Router();

//listar faturas
router.get('/', async (req, res) => {
    try {
        const faturas = await Fatura.find()

        return res.send({ faturas })
    } catch (err) {
        return res.status(400).send({ Erro: 'Erro ao listar faturas' })
    }
});

//consultar fatura
router.get('/:idfatura', async (req, res) => {
    try {
        const fatura = await Fatura.findOne({ idfatura: req.params.idfatura })
        return res.send({ fatura })
    } catch (err) {
        return res.status(400).send({ Erro: 'Erro ao consultar faturas' })
    }
})

//criar fatura
router.post('/', async (req, res) => {
    try {
        const fatura = await Fatura.create(req.body)
        return res.send({ fatura })
    } catch (err) {
        return res.status(400).send({ Erro: 'Erro ao criar faturas' })
    }
})

//editar fatura
router.put('/:idfatura', async (req, res) => {
    try {
        const fatura = await Fatura.updateOne({ idfatura: req.params.idfatura }, { ...req.body }, { runValidators: true })
        return res.send({ fatura })
    } catch (err) {
        return res.status(400).send({ Erro: 'Erro ao editar faturas' })
    }
})

//deletar fatura
router.delete('/:idfatura', async (req, res) => {
    try {
        await Fatura.findOneAndDelete({ idfatura: req.params.idfatura })
        return res.send(204)
    } catch (err) {
        return res.status(400).send({ Erro: 'Erro ao deletar faturas' })
    }
})

module.exports = app => app.use('/fatura', router)